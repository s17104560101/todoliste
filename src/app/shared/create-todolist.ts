import {Todolist} from "./todolist";

export class CreateTodoList {

    static empty():Todolist {
        return new Todolist(0,"",[],
            new Date());
    }
    static fromObject(rawShoppinglist:any) : Todolist {
        return new Todolist(
            rawShoppinglist.id,
            rawShoppinglist.title,
            rawShoppinglist.entries,
            rawShoppinglist.duedate,
        );
    }
}