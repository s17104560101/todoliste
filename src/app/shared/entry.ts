
export class Entry{
    constructor (
        public id: string,
        todolist_id:number,
        public text: string,
        public done: boolean,

    ) { }
}