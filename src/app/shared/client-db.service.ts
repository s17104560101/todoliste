import { Injectable } from '@angular/core';
import Dexie from "dexie";
import {Todolist} from "./todolist";
import {Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ClientDbService extends Dexie{

    todolists:Dexie.Table<Todolist, string>;
    constructor() {
        super('todolistdb');
        this.version(1).stores({
            todolists: '++id, title, *entries, duedate',
            entries: '++id, title, *entries, duedate',
        });


        this.todolists.add({id: 1, title: 'Studium-Aufgaben', entries:[{id:"1",text:'Wissenschaftliches Arbeiten UE03',done:true}, {id:"2",text:'NodeJS',done:false}], duedate:new Date()});
        this.todolists.add({id: 2, title: 'Hausarbeit', entries:[{id:"1",text:'Saugen',done:true}, {id:"2",text:'Geschirrspüler',done:false}], duedate:new Date()});

    }

    create(todolist: Todolist) {
        this.todolists.add(todolist);
        return true;
    }








}