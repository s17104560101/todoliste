import {Entry} from "./entry";
export class Todolist {
    constructor (
        public id: number,
        public title: string,
        public entries:Entry[],
        public duedate?: Date,

    ) { }
}
