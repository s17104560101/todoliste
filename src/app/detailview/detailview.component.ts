import {Component, OnInit} from '@angular/core';
import {ClientDbService} from "../shared/client-db.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import { MatListOption } from '@angular/material/list';
import {v4 as uuidv4} from 'uuid';
import {Entry} from "../shared/entry";



@Component({
    selector: 'app-detailview',
    templateUrl: './detailview.component.html',
    styleUrls: ['./detailview.component.css']
})


export class DetailviewComponent implements OnInit {

    todolist;
    entriesNotDone = [];
    entries = [];
    params;
    makeEntry: FormGroup;

    constructor(private formbuilder: FormBuilder, private clientDbService: ClientDbService, private route: ActivatedRoute, private router: Router,) {
    }

    ngOnInit(): void {
        this.makeEntry = this.formbuilder.group({
            id: uuidv4(),
            text: '',
            done: false,
        });

        this.params = this.route.snapshot.params;
        const id = Number(this.params['id']);
        this.todolist = this.getList(id);
    }



    addEntry(){
        this.clientDbService.todolists.where('id').equals(Number(this.params['id'])).modify(todolist => {todolist.entries.push(this.makeEntry.value)});
        console.log("added Entry to cdb");
        window.location.reload();
    }

    getList(id) {
        this.todolist = this.clientDbService.todolists.where('id').equals(id).first(todolist => this.todolist = todolist);
    }

    changeTodo(entries: MatListOption[]){
        let entriesArrayDone = [];

        entries.map(o => o.value.done = true);
        for(let entry of entries){
            entriesArrayDone.push(entry);
        }

        for(let entry of this.todolist.entries){
            this.todolist.entries.map(entry => entriesArrayDone.find(e => e.id === entry.id) || entry);
        }

        console.log(this.todolist.entries);
        this.updateEntries(this.todolist.entries);
    }

    updateEntries(entries){
        this.clientDbService.todolists.where('id').equals(Number(this.params['id'])).modify(todolist => {todolist.entries = entries});

    }


}
