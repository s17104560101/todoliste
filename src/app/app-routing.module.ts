import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListviewComponent} from "./listview/listview.component";
import {DetailviewComponent} from "./detailview/detailview.component";
import {CreateComponent} from "./create/create.component";


const routes: Routes = [
    {path:'',redirectTo:'todolists', pathMatch:'full'},
    {path:'todolists',component:ListviewComponent},
    {path:'todolists/:id', component:DetailviewComponent},
    {path: 'create', component:CreateComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
