import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Todolist} from "../shared/todolist";
import {ClientDbService} from "../shared/client-db.service";


@Component({
  selector: 'app-listview',
  templateUrl: './listview.component.html',
  styleUrls: ['./listview.component.css']
})
export class ListviewComponent implements OnInit {

  todolists: Todolist[];

  constructor(private  router: Router,  private clientDbService:ClientDbService) { }

  ngOnInit(): void {
    this.updateTodolists();
  }

    detailView(id){
        this.router.navigateByUrl('/todolists/' + id);
    }


  async updateTodolists(){
    this.todolists = await this.clientDbService.todolists.toArray();
  }

    createView() {
        this.router.navigateByUrl('create');
    }

}

