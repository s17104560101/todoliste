import { Component, OnInit } from '@angular/core';

import {CreateTodoList} from '../shared/create-todolist'
import{ActivatedRoute, Router} from "@angular/router";

import {FormGroup, FormBuilder, Validators, FormArray} from "@angular/forms";
import {Todolist} from "../shared/todolist";
import {ClientDbService} from "../shared/client-db.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    todolist = CreateTodoList.empty();
    makeForm: FormGroup;
    entries: FormArray;

  constructor(private formbuilder: FormBuilder, private cdb: ClientDbService,  private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

      this.makeForm = this.formbuilder.group({
          title: '',
          duedate: [''],
          entries: this.formbuilder.array([this.createEntry()]),
      });
  }


    createEntry(): FormGroup {
        return this.formbuilder.group({
            text: '',
            done: false,
        });
    }

    addEntry(): void {
        this.entries = this.makeForm.get('entries') as FormArray;
        this.entries.push(this.createEntry());
        console.log(this.entries);
        if (this.entries.controls[0]) {
            // console.log(this.items.controls[0].controls.value);
        }
    }

        getformData() {
            return <FormArray>this.makeForm.get('entries');
        }


        save(){
            let todolist:Todolist = CreateTodoList.fromObject(this.makeForm.value);
            const val = this.makeForm.value;
            todolist=val;

            if(this.cdb.create(todolist)){
                this.todolist = CreateTodoList.empty();
                this.makeForm.reset(CreateTodoList.empty());
                this.router.navigate(['/']);
            }
        }



}
